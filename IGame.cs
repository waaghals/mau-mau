﻿using System.Collections.Generic;

namespace Waaghals.Cards
{
    interface IGame
    {
        IEnumerator<string> CreateGame();
    }
}
