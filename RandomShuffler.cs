﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Waaghals.Cards
{
    //https://stackoverflow.com/a/1653204
    class FisherYatesDurstenfeldShuffler<T> : IShuffer<T>
    {
        public IEnumerable<T> Shuffle(IEnumerable<T> values)
        {
            if (values == null)
            {
                throw new ArgumentNullException(nameof(values));
            }

            return Iterator(values, new Random());
        }

        private static IEnumerable<T> Iterator(IEnumerable<T> source, Random rng)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (rng == null)
            {
                throw new ArgumentNullException(nameof(rng));
            }

            var buffer = source.ToList();
            for (int i = 0; i < buffer.Count; i++)
            {
                int j = rng.Next(i, buffer.Count);
                yield return buffer[j];

                buffer[j] = buffer[i];
            }
        }
    }
}
