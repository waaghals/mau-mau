﻿using Waaghals.Cards.PlayingCard;

namespace Waaghals.Cards
{
    interface ICardAccepter
    {
        void Add(Card card);
    }
}
