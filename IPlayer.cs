﻿using System.Collections.Generic;
using Waaghals.Cards.PlayingCard;


namespace Waaghals.Cards
{
    interface IPlayer : ICardAccepter
    {
        void Reset();
        void Remove(Card card);

        string Name { get; }
        IEnumerable<Card> CardsInHand { get; }
    }
}
