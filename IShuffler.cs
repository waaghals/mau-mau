﻿using System.Collections.Generic;

namespace Waaghals.Cards
{
    interface IShuffer<T>
    {
        IEnumerable<T> Shuffle(IEnumerable<T> values);
    }
}
