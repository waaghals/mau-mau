﻿using System.Diagnostics;

namespace Waaghals.Cards.PlayingCard
{
    [DebuggerDisplay("{Face} of {Suit}")]
    class Card
    {
        public Card(Suit suit, Face face)
        {
            Suit = suit;
            Face = face;
        }

        public Suit Suit { get; }
        public Face Face { get; }
    }
}
