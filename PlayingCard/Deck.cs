﻿using System.Collections.Generic;
using Waaghals.Cards.Helpers;

namespace Waaghals.Cards.PlayingCard
{
    class Deck
    {
        public IEnumerable<Card> Cards { get; }
        public Deck()
        {
            Cards = Build();
        }

        private static IEnumerable<Card> Build()
        {
            foreach (var suit in EnumUtils.Values<Suit>())
            {
                foreach (var face in EnumUtils.Values<Face>())
                {
                    yield return new Card(suit, face);
                }
            }
        }
    }
}
