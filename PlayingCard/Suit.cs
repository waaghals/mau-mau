﻿namespace Waaghals.Cards.PlayingCard
{
    enum Suit
    {
        Hearts,
        Spades,
        Clubs,
        Diamonds
    }
}
