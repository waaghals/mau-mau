﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Waaghals.Cards.PlayingCard
{
    [DebuggerDisplay("TopCard = {TopCard.Face} of {TopCard.Suit}")]
    class Pile : ICardAccepter
    {
        private readonly IList<Card> discarded;
        public Card TopCard { get; private set; }

        public Pile(Card initialCard)
        {
            TopCard = initialCard ?? throw new ArgumentNullException(nameof(initialCard));
            discarded = new List<Card>(new Card[] { initialCard });
        }

        public void Add(Card card)
        {
            TopCard = card ?? throw new ArgumentNullException(nameof(card));

            discarded.Add(card);
        }
    }
}
