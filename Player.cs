﻿using System.Collections.Generic;
using System.Diagnostics;
using Waaghals.Cards.PlayingCard;

namespace Waaghals.Cards
{
    [DebuggerDisplay("Name = {Name}")]
    class Player : IPlayer
    {
        public string Name { get; }
        private IList<Card> cardsInHand;
        public IEnumerable<Card> CardsInHand => cardsInHand;

        public Player(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new System.ArgumentException("Name cannot be empty", nameof(name));
            }

            Name = name;
            cardsInHand = new List<Card>();
        }

        public void Add(Card card)
        {
            cardsInHand.Add(card);
        }

        public void Remove(Card card)
        {
            cardsInHand.Remove(card);
        }

        public void Reset()
        {
            cardsInHand = new List<Card>();
        }
    }
}
