﻿using System;
using System.Collections.Generic;
using System.Linq;
using Waaghals.Cards.MauMau.Event;
using Waaghals.Cards.PlayingCard;

namespace Waaghals.Cards
{
    class MauMauGame : IGame
    {
        private const short initialCardsPerPlayer = 7;
        private readonly IEnumerable<MauMauPlayer> players;
        private readonly IEnumerable<Deck> decks;
        private readonly IShuffer<Card> shuffler;
        private readonly IList<string> gameStates;
        private bool finished;

        public MauMauGame(IEnumerable<IPlayer> players, IEnumerable<Deck> decks, IShuffer<Card> shuffler)
        {
            //Keep a static list of players, keeping just a IEnumrable will yield new players each time iteration is started.
            this.players = players.Select(p => new MauMauPlayer(p)).ToArray();
            InitializeEventHandlers();
            this.decks = decks;
            this.shuffler = shuffler;
            this.gameStates = new List<string>();
        }

        private void InitializeEventHandlers()
        {
            foreach (var player in players)
            {
                player.SecondToLastCardPlayed += new EventHandler<SecondToLastCardPlayed>(HandleSecondToLastCardPlayed);
                player.StackEmptied += new EventHandler<StackEmptied>(HandleStackEmptied);
                player.CardPlayed += new EventHandler<CardPlayed>(HandleCardPlayed);
                player.CardTaken += new EventHandler<CardTaken>(HandleCardTaken);
            }
        }

        private Stack<Card> Deal()
        {
            var stack = new Stack<Card>();
            var allCards = decks.SelectMany(d => d.Cards);
            var shuffledCards = shuffler.Shuffle(allCards);
            var enumerator = shuffledCards.GetEnumerator();

            for (int i = 0; i < initialCardsPerPlayer; i++)
            {
                foreach (var player in players)
                {
                    enumerator.MoveNext();
                    player.Add(enumerator.Current);
                }
            }

            //No longer dealing to players
            //Add remaining cards to the stack
            while (enumerator.MoveNext())
            {
                stack.Push(enumerator.Current);
            }

            return stack;
        }

        private void HandleSecondToLastCardPlayed(object sender, SecondToLastCardPlayed secondToLastCardPlayed)
        {
            gameStates.Add($"{secondToLastCardPlayed.Player.Name} has only a single card left!");
        }

        private void HandleStackEmptied(object sender, StackEmptied stackEmptied)
        {
            gameStates.Add($"{stackEmptied.Player.Name} emptied the stack of cards!");
            finished = true;
        }

        private void HandleCardPlayed(object sender, CardPlayed cardPlayed)
        {
            var player = cardPlayed.Player;
            var card = cardPlayed.Card;
            gameStates.Add($"{player.Name} added {card.Face} of {card.Suit} to the pile.");
            if (!player.CardsInHand.Any())
            {
                gameStates.Add($"{player.Name} won the game!");
                finished = true;
            }
        }

        private void HandleCardTaken(object sender, CardTaken cardTaken)
        {
            var card = cardTaken.Card;
            gameStates.Add($"{cardTaken.Player.Name} took {card.Face} of {card.Suit} from the stack.");
        }

        public IEnumerator<string> CreateGame()
        {
            ResetGame();

            var stack = Deal();
            var pile = new Pile(stack.Pop());
            foreach (var state in DescribeNewGame(pile.TopCard))
            {
                yield return state;
            }

            while (true)
            {
                foreach (var player in players)
                {
                    yield return "";

                    player.Play(pile, stack);

                    //All events have fired, and where collected, yield a description of each events
                    foreach (var gameState in gameStates)
                    {
                        yield return gameState;
                    }
                    gameStates.Clear();

                    if (finished)
                    {
                        yield break;
                    }
                }
            }
        }

        private void ResetGame()
        {
            finished = false;
            ResetPlayers();
        }

        private void ResetPlayers()
        {
            foreach (var player in players)
            {
                player.Reset();
            }
        }

        private IEnumerable<string> DescribeNewGame(Card topCard)
        {
            yield return "New game started.";
            foreach (var player in players)
            {
                foreach(var card in player.CardsInHand)
                {
                    yield return $"{player.Name} was dealt {card.Face} of {card.Suit}.";
                }

                yield return "";
            }
            yield return $"Top card is {topCard.Face} of {topCard.Suit}.";
        }
    }
}
