﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Waaghals.Cards.MauMau.Event;
using Waaghals.Cards.PlayingCard;

namespace Waaghals.Cards
{
    [DebuggerDisplay("Name = {Name}")]
    class MauMauPlayer : IPlayer
    {
        public event EventHandler<CardTaken> CardTaken;
        public event EventHandler<CardPlayed> CardPlayed;
        public event EventHandler<StackEmptied> StackEmptied;
        public event EventHandler<SecondToLastCardPlayed> SecondToLastCardPlayed;

        private readonly IPlayer player;

        public string Name => player.Name;
        public IEnumerable<Card> CardsInHand => player.CardsInHand;

        public MauMauPlayer(IPlayer player)
        {
            this.player = player ?? throw new ArgumentNullException(nameof(player));
        }

        public void Play(Pile pile, Stack<Card> stack)
        {
            if (pile == null)
            {
                throw new ArgumentNullException(nameof(pile));
            }

            if (stack == null)
            {
                throw new ArgumentNullException(nameof(stack));
            }

            var topCard = pile.TopCard;
            foreach (var cardInHand in player.CardsInHand)
            {
                if (topCard.Face != cardInHand.Face && topCard.Suit != cardInHand.Suit)
                {
                    continue;
                }
                PlayCard(pile, cardInHand);
                return;
            }

            //No Matching card found in hand, pick one from the stack, and try again
            TakeCardFromStack(stack);

            //Try again
            Play(pile, stack);
        }

        private void TakeCardFromStack(Stack<Card> stack)
        {
            var pickedCard = stack.Pop();
            player.Add(pickedCard);

            OnCardTaken(pickedCard);
            if (!stack.TryPeek(out _))
            {
                OnStackEmptied();
                return;
            }
        }

        private void PlayCard(Pile pile, Card cardInHand)
        {
            player.Remove(cardInHand);
            pile.Add(cardInHand);

            OnCardPlayed(cardInHand);

            CheckLastCard();
        }

        private void CheckLastCard()
        {
            if (player.CardsInHand.Count() == 1)
            {
                OnSecondToLastCardPlayed();
            }
        }

        private void OnSecondToLastCardPlayed()
        {
            var e = new SecondToLastCardPlayed(this);
            SecondToLastCardPlayed?.Invoke(this, e);
        }

        private void OnStackEmptied()
        {
            var e = new StackEmptied(this);
            StackEmptied?.Invoke(this, e);
        }

        private void OnCardTaken(Card card)
        {
            var e = new CardTaken(player, card);
            CardTaken?.Invoke(this, e);
        }

        private void OnCardPlayed(Card card)
        {
            var e = new CardPlayed(player, card);
            CardPlayed?.Invoke(this, e);
        }

        public void Reset()
        {
            player.Reset();
        }

        public void Add(Card card)
        {
            player.Add(card);
        }

        public void Remove(Card card)
        {
            player.Remove(card);
        }
    }
}
