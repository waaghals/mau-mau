﻿using System;

namespace Waaghals.Cards.MauMau.Event
{
    class SecondToLastCardPlayed : EventArgs
    {
        public IPlayer Player { get; }

        public SecondToLastCardPlayed(IPlayer player)
        {
            Player = player ?? throw new ArgumentNullException(nameof(player));
        }
    }
}
