﻿using Waaghals.Cards.PlayingCard;
using System;

namespace Waaghals.Cards.MauMau.Event
{
    class CardPlayed : EventArgs
    {
        public IPlayer Player { get; }
        public Card Card { get; }

        public CardPlayed(IPlayer player, Card card)
        {
            Player = player ?? throw new ArgumentNullException(nameof(player));
            Card = card ?? throw new ArgumentNullException(nameof(card));
        }
    }
}
