﻿using System;
using Waaghals.Cards.PlayingCard;

namespace Waaghals.Cards.MauMau.Event
{
    class CardTaken : EventArgs
    {
        public IPlayer Player { get; }
        public Card Card { get; }
        public CardTaken(IPlayer player, Card card)
        {
            Player = player ?? throw new ArgumentNullException(nameof(player));
            Card = card ?? throw new ArgumentNullException(nameof(card));
        }
    }
}
