﻿using System;

namespace Waaghals.Cards.MauMau.Event
{
    class StackEmptied : EventArgs
    {
        public IPlayer Player { get; }

        public StackEmptied(IPlayer player)
        {
            Player = player ?? throw new ArgumentNullException(nameof(player));
        }
    }
}
