﻿using System;
using System.Linq;
using Waaghals.Cards.PlayingCard;

namespace Waaghals.Cards
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new string[] { "Jan", "Wilbert", "Robin", "Stijn" };
            var players = names.Select(n => new Player(n));
            var decks = new Deck[] { new Deck(), new Deck() };
            var shuffler = new FisherYatesDurstenfeldShuffler<Card>();

            var maumau = new MauMauGame(players, decks, shuffler);
            var game = maumau.CreateGame();

            while (Console.ReadKey(true).Key != ConsoleKey.Escape)
            {
                var gameState = game.Current;
                Console.WriteLine(gameState);
                if (!game.MoveNext())
                {
                    game = maumau.CreateGame();
                }
            }
        }
    }
}
