﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Waaghals.Cards.Helpers
{
    public static class EnumUtils
    {
        public static IEnumerable<T> Values<T>() where T : struct
        {
            var type = typeof(T);
            if (!type.IsEnum)
            {
                throw new ArgumentException("Generic type is not an Enum");
            }

            return Enum.GetValues(typeof(T)).Cast<T>();
        }
    }
}
